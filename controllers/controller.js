const service = require('../services/service');

async function uploadXML(req,res){
    if (!req.file) {
        res.status(404).send('No file uploaded.');
    }
    else if (req.file.mimetype !== 'application/xml'){
        res.status(400).send('The uploaded file is not an xml file.');
    }
    else{
        console.log(req.file);
        res.send('File uploaded successfully!');
    }
}

async function validateItemsExistsT1(req,res){
    if (checkFileUploaded(req,res)){
        try{
            await service.validateItemsExistsT1(req.file)
                .then( () => res.send("Entity, Attribute and Relation Exists!")
                );
        }catch (e){
            console.log(e)
            res.status(400).send({error: e.message});
        }
    }
}

async function validateConnectionsExistsT2(req,res){
    if (checkFileUploaded(req,res)){
        try{
            await service.validateConnectionsExistsT2(req.file)
                .then( () => res.send("Connections between entity, attributes and relationships are valid!")
                );
        }catch (e){
            console.log(e)
            res.status(400).send({error: e.message});
        }
    }
}

async function validateAllTests(req,res){
    if (checkFileUploaded(req,res)){
        return true;
    }
}

function checkFileUploaded(req,res){
    if (!req.file) { res.status(404).send('No file uploaded.'); return false}
    if (req.file.mimetype !== 'application/xml'){ res.status(400).send('The uploaded file is not an xml file.'); return false}
    return true;
}

async function uploadJSON(req,res){
    if (!req.file) {
        res.status(404).send('No file uploaded.');
    }
    else if (req.file.mimetype !== 'application/json'){
        res.status(400).send('The uploaded file is not an json file.');
    }
    else{
        console.log(req.file);
        res.send('File uploaded successfully!');
    }
}

module.exports = {
    uploadXML,
    uploadJSON,
    validateItemsExistsT1,
    validateConnectionsExistsT2,
    validateAllTests
}
