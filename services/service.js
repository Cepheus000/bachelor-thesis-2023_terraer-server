const xml2js = require('xml2js');
const fs = require('fs');

/*
Test 1:
Is there an Entity / attribute / Relation.
*/
async function validateItemsExistsT1(file){
    console.log("Starting test validateItemsExistsT1...")
    const parser = new xml2js.Parser();
    const xmlString = fs.readFileSync(file.path, 'utf8');


    await checkEntityExists(parser,xmlString);
    await checkAttributeExists(parser,xmlString);
    await checkRelationExists(parser,xmlString);
}

async function checkEntityExists(parser,xmlString){
    let entities;

    try {
        const result = await parser.parseStringPromise(xmlString);
        entities = result.drawing.figures[0].ent;
    } catch (e) {
        console.log(e);
        throw new Error('Error parsing XML file. (entity exists test)');
    }
    if (!entities || entities.length === 0) {
        throw new Error('No entities found in given XML file.');
    }
    console.log('1) An Entity exists!');
}

async function checkAttributeExists(parser,xmlString){
    let attributes;

    try {
        const result = await parser.parseStringPromise(xmlString);
        attributes = result.drawing.figures[0].atr;
    } catch (e) {
        console.log(e);
        throw new Error('Error parsing XML file. (attribute exists test)');
    }
    if (!attributes || attributes.length === 0) {
        throw new Error('No attributes found in given XML file.');
    }
    console.log('2) An Attribute exists!');
}

async function checkRelationExists(parser,xmlString){
    let relations;

    try {
        const result = await parser.parseStringPromise(xmlString);
        relations = result.drawing.figures[0].rel;
    } catch (e) {
        console.log(e);
        throw new Error('Error parsing XML file. (relations exists test)');
    }
    if (!relations || relations.length === 0) {
        throw new Error('No relations found in given XML file.');
    }
    console.log('3) A Relation exists!');
}
/*
END TEST 1
---------
V2:
Is there connection between attribute and Entity + Entity and Relation.

*/

async function validateConnectionsExistsT2(file){
    console.log("Starting test validateConnectionsExistsT2...")
    const parser = new xml2js.Parser();
    const xmlString = fs.readFileSync(file.path, 'utf8');

    try{
        const result = await parser.parseStringPromise(xmlString);
        await attributesConnections(result);

    }catch (e) {
        console.log(e);
        throw new Error('Error parsing XML file. (attributes connected test) ' + e);
    }
}

async function attributesConnections(xmlData) {
    const attributes = xmlData.drawing.figures[0].atr;
    const connectors = xmlData.drawing.figures[0].lcaf;

    if (!connectors) {
        if (attributes.length > 0) {
            const unconnectedAttributes = attributes.map(a => a.$.id);
            throw new Error(`The following attributes are not connected to any entity: ${unconnectedAttributes.join(", ")}`);
        } else {
            return true;
        }
    }

    const attributeMap = {};
    const unconnectedAttributes = [];

    // Build a map of attributes and their connectors
    for (const attribute of attributes) {
        const attributeId = attribute.$.id;
        const connector = connectors.find(c =>
            c.startConnector && c.startConnector[0].rConnector &&
            c.startConnector[0].rConnector[0].Owner &&
            c.startConnector[0].rConnector[0].Owner[0].atr &&
            c.startConnector[0].rConnector[0].Owner[0].atr[0].$.ref === attributeId
        );
        if (connector) {
            const entityId = connector.endConnector[0].rConnector[0].Owner[0].ent[0].$.ref;
            if (attributeMap[attributeId] && attributeMap[attributeId] !== entityId) {
                throw new Error(`Attribute ${attributeId} is connected to multiple entities`);
            } else {
                attributeMap[attributeId] = entityId;
            }
        } else {
            unconnectedAttributes.push(attributeId);
        }
    }

    if (unconnectedAttributes.length === 0) {
        console.log('4) All attributes are connected!');
        return true;
    } else {
        throw new Error(`The following attributes are not connected to any entity: ${unconnectedAttributes.join(", ")}`);
    }
}

async function validateERDT3(file){
    /* Is there an Entity / attribute / Relation + are they connected? + keys
    So at least 2 entities with both an id and relationship between them */
}




module.exports = {
    validateItemsExistsT1,
    validateConnectionsExistsT2
}
