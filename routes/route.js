const express = require('express');
const router = express.Router();
const controller = require('../controllers/controller');
const multer = require("multer");
const upload = multer({ dest: 'uploads/' });

router.post('/upload-xml', upload.single('file'), controller.uploadXML);
router.post("/validate-erd-t1", upload.single('file'),controller.validateItemsExistsT1)
router.post("/validate-erd-t2", upload.single('file'),controller.validateConnectionsExistsT2)

module.exports = router;